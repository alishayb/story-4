from django.test import TestCase, Client

# Create your tests here.
class BooksSearchTest(TestCase):
	def test_url_books_exist(self):
		response = Client().get('/books/')
		self.assertEquals(response.status_code, 200)

	def test_books_html_is_used(self):
		response = Client().get('/books/')
		self.assertTemplateUsed(response, 'story8.html')