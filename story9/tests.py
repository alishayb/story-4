from django.test import TestCase, Client

# Create your tests here.
class LoginTest(TestCase):
	def test_url_books_exist(self):
		response = Client().get('/login/')
		self.assertEquals(response.status_code, 200)

	def test_url_books_exist(self):
		response = Client().get('/logout/')
		self.assertEquals(response.status_code, 200)

	def test_url_books_exist(self):
		response = Client().get('/register/')
		self.assertEquals(response.status_code, 200)

	def test_books_html_is_used(self):
		response = Client().get('/login/')
		self.assertTemplateUsed(response, 'login.html')

	def test_books_html_is_used(self):
		response = Client().get('/logout/')
		self.assertTemplateUsed(response, 'index.html')

	def test_books_html_is_used(self):
		response = Client().get('/register/')
		self.assertTemplateUsed(response, 'register.html')

