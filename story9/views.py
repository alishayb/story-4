from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm

from django.contrib.auth import authenticate
from django.contrib.auth import login as django_login
from django.contrib.auth import logout as django_logout

from django.contrib import messages

from django.contrib.auth.decorators import login_required

from .forms import CreateUserForm

# Create your views here.
def login(request):
	if request.user.is_authenticated:
		return redirect('/welcome/')
	else:
		if request.method == 'POST':
			print("hm")
			username = request.POST.get('username')
			password =request.POST.get('password')

			user = authenticate(request, username=username, password=password)

			if user is not None:
				django_login(request, user)
				return redirect('/welcome/')
			else:
				messages.info(request, 'Username OR password is incorrect')

		response = {}
		return render(request, 'login.html',response)

def logout(request):
	django_logout(request)
	return redirect('app_ppw:index')

def register(request):
	if request.user.is_authenticated:
		return redirect('/index/')
	else:	
		form = CreateUserForm()
		if request.method == 'POST':
			form = CreateUserForm(request.POST)
			if form.is_valid():
				form.save()
				user = form.cleaned_data.get('username')

				return redirect('/welcome/')

		response = {'form': form}
		return render(request, 'register.html', response)


@login_required(login_url='login')
def welcome(request):
	return render(request, 'welcome.html')