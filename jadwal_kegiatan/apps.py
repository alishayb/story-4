from django.apps import AppConfig


class JadwalKegiatanConfig(AppConfig):
    name = 'jadwal_kegiatan'
