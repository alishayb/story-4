from django.db import models

# Create your models here.
class JadwalKegiatan(models.Model):
    nama_Kegiatan = models.CharField(max_length=100, null=True)

class Peserta(models.Model):
    nama_Peserta = models.CharField(max_length=100, null=True)
    kegiatan = models.ForeignKey(to=JadwalKegiatan, on_delete = models.CASCADE, null=True)