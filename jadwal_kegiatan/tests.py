from django.test import TestCase, Client
from django.urls import reverse
from .models import JadwalKegiatan, Peserta

# Create your tests here.
class JadwalKegiatanUnitTest(TestCase):
    def test_list_kegiatan_exist(self):
        response = Client().get('/jadwal_kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_tambah_kegiatan_exist(self):
        response = Client().get('/tambah_kegiatan/')
        self.assertEquals(response.status_code, 200)

    def test_tambah_peserta_exist(self):
        response = Client().get(reverse('app_ppw:tambah_peserta', args=['1']))
        self.assertEquals(response.status_code, 200)

    def test_book_search_exist(self):
        response = Client().get('/books/')
        self.assertEquals(response.status_code, 200)

    def test_create_kegiatan(self):
        JadwalKegiatan.objects.create(nama_Kegiatan = 'Kegiatan1')
        count_object = JadwalKegiatan.objects.all().count()
        self.assertEquals(count_object, 1)
    
    def test_create_peserta(self):
        Peserta.objects.create(nama_Peserta = 'Peserta1')
        count_object = Peserta.objects.all().count()
        self.assertEquals(count_object, 1)

    
    
