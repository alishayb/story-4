from django import forms
from .models import JadwalKegiatan, Peserta

class Input_Kegiatan(forms.ModelForm):
    class Meta:
        model = JadwalKegiatan
        fields = [
            'nama_Kegiatan',  
        ]
        widgets = {
            'nama_Kegiatan' : forms.TextInput(attrs={'class':'form-style mb-5'}),
        }

class Input_Peserta(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'nama_Peserta',  
        ]
        widgets = {
            'nama_Peserta' : forms.TextInput(attrs={'class':'form-style mb-5'}),
        }

