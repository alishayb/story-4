from django.contrib import admin
from jadwal_kegiatan.models import JadwalKegiatan, Peserta

# Register your models here.
admin.site.register(JadwalKegiatan)
admin.site.register(Peserta)
