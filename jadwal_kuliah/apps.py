from django.apps import AppConfig


class JadwalKuliahConfig(AppConfig):
    name = 'jadwal_kuliah'
