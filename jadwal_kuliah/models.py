from django.db import models
from django.core.validators import MaxValueValidator, MinValueValidator 

# Create your models here.
class MataKuliah(models.Model):
    nama_Mata_Kuliah = models.CharField(max_length=100, null=True)
    dosen_Pengajar = models.CharField(max_length=100, null=True)
    SKS = models.IntegerField(null=True, validators=[MinValueValidator(1), MaxValueValidator(10)])
    deskripsi = models.CharField(max_length=700, null=True)
    SEMESTER = (
        ('Gasal', 'Gasal'),
        ('Genap', 'Genap'),
    )
    semester = models.CharField(max_length=5, choices=SEMESTER, null=True)
    tahun_Ajaran = models.CharField(max_length=10, null=True)
    ruang_Kelas = models.CharField(max_length=10, null=True)

    # Auto capitalize for every word of nama mata kuliah and dosen pengajar
    def save(self, *args, **kwargs):
        fields = ['nama_Mata_Kuliah', 'dosen_Pengajar']
        for field_name in fields:
            val = getattr(self, field_name, False)
            if val:
                split_val = val.split(" ")
                for i in range(len(split_val)):
                    split_val[i] = split_val[i].capitalize()
                
                new_val = " ".join(split_val)
                setattr(self, field_name, new_val)

        super(MataKuliah, self).save(*args, **kwargs)