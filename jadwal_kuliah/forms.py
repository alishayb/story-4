from django import forms
from .models import MataKuliah

class Input_Form(forms.ModelForm):
    class Meta:
        model = MataKuliah
        fields = [
            'nama_Mata_Kuliah', 
            'dosen_Pengajar', 
            'SKS', 
            'deskripsi', 
            'semester', 
            'tahun_Ajaran',
            'ruang_Kelas',
        ]
        widgets = {
            'nama_Mata_Kuliah' : forms.TextInput(attrs={'class':'form-style mb-5'}),
            'dosen_Pengajar' : forms.TextInput(attrs={'class':'form-style mb-5'}),
            'SKS': forms.NumberInput(
                    attrs={
                        'class':'form-style mb-5',
                        'placeholder': 'Range 1-10'
                    }),
            'deskripsi' : forms.Textarea(
                            attrs={
                                'class':'form-style mb-5', 
                                'id':'deskripsi',
                                'rows': 10,
                                'cols': 50,
                                'placeholder': 'Deskripsi mengenai mata kuliah',
                            }),
            'semester' : forms.Select(attrs={'class':'form-style mb-5', 'id':'semester'}),
            'tahun_Ajaran' : forms.TextInput(attrs={'class':'form-style mb-5'}),
            'ruang_Kelas' : forms.TextInput(attrs={'class':'form-style mb-5'}),
        }


