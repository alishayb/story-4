from django.test import TestCase, Client

# Create your tests here.
class GeneralTest(TestCase):
    # home
    def test_url_home_exist(self):
        response = Client().get('')
        self.assertEquals(response.status_code, 200)

    def test_base_is_used(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'base.html')

    def test_home_html_is_used(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'index.html')

    def test_information_in_home(self):
        response = Client().get('')
        self.assertContains(response, "Alisha")
        self.assertContains(response, 'PREVIOUS PROJECT')
        self.assertContains(response, 'EXPERIENCE')
        self.assertContains(response, 'BIODATA')
        self.assertContains(response, 'EDUCATION')
        img_src = 'images/JP04-smaller.png';
        self.assertContains(response, img_src)

    # story 1
    def test_url_story1_exist(self):
        response = Client().get('/story1')
        self.assertEquals(response.status_code, 200)

    def test_story1_html_is_used(self):
        response = Client().get('/story1')
        self.assertTemplateUsed(response, 'story1.html')


    # about
    def test_url_about_exist(self):
        response = Client().get('/about/')
        self.assertEquals(response.status_code, 200)

    def test_about_html_is_used(self):
        response = Client().get('/about/')
        self.assertTemplateUsed(response, 'about.html')

    def test_information_in_about(self):
        response = Client().get('/about/')
        self.assertContains(response, "About Me")

    def test_accordion_information(self):
        response = Client().get('/about/')
        self.assertContains(response, "Aktivitas Saat Ini")
        self.assertContains(response, "Pengalaman Organisasi")
        self.assertContains(response, "Pengalaman Kepanitiaan")
        self.assertContains(response, "Prestasi")


    


    
    
