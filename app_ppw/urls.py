from django.urls import path
from . import views

app_name = 'app_ppw'

urlpatterns = [
    path('', views.index, name='index'),
    path('about/', views.about, name='about'),
    path('story1', views.story1, name='story1'),

    path('add_kuliah', views.add_kuliah, name='add_kuliah'),
    path('list_kuliah/', views.list_kuliah, name='list_kuliah'),
    path('detail_kuliah/<str:id>', views.detail_kuliah, name='detail_kuliah'),
    path('delete_kuliah/<str:id>', views.delete_kuliah, name='delete_kuliah'),
    path('list_kuliah/<str:id>', views.delete_kuliah_confirm, name='delete_kuliah_confirm'),

    path('jadwal_kegiatan/', views.list_event, name='jadwal_kegiatan'),
    path('tambah_kegiatan/', views.add_event, name='tambah_kegiatan'),
    path('tambah_peserta/<str:id>/', views.add_member, name='tambah_peserta'),
]