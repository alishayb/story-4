from django.shortcuts import render, redirect
from jadwal_kuliah.forms import Input_Form
from jadwal_kuliah.models import MataKuliah
from jadwal_kegiatan.models import JadwalKegiatan, Peserta
from jadwal_kegiatan.forms import Input_Kegiatan, Input_Peserta

# Create your views here.
def index(request):
    return render(request, 'index.html')

def about(request):
    return render(request, 'about.html')

def story1(request):
    return render(request, 'story1.html')

# Fitur List Kuliah
def add_kuliah(request):
    form = Input_Form(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('app_ppw:list_kuliah')
    else:
        form=Input_Form()
    
    response = {
        'form' : form
    }

    return render(request, 'jadwal_kuliah.html', response)

def list_kuliah(request):
    mata_kuliah = MataKuliah.objects.all()
    response = {
        'mata_kuliah' : mata_kuliah
    }

    return render(request, 'list_kuliah.html', response)

def detail_kuliah(request, id):
    response = {
        'kuliah' : MataKuliah.objects.get(pk=id)
    }
    return render(request, 'detail_kuliah.html', response)

def delete_kuliah(request, id):
    response = {
        'kuliah' : MataKuliah.objects.get(pk=id)
    }
    return render(request, 'delete_kuliah.html', response)

def delete_kuliah_confirm(request, id):
    MataKuliah.objects.get(pk=id).delete()
    mata_kuliah = MataKuliah.objects.all()
    response = {
        'mata_kuliah' : mata_kuliah
    }

    return render(request, 'list_kuliah.html', response)

# Fitur List Event
def list_event(request):
    kegiatan = JadwalKegiatan.objects.all()
    member = Peserta.objects.all()
    response = {
        'list_kegiatan' : kegiatan,
        'list_member' : member,
    }

    return render(request, 'jadwal_kegiatan.html', response)

def add_event(request):
    form = Input_Kegiatan(request.POST or None) 
    if form.is_valid():
        form.save()
        form = Input_Kegiatan()
        
    else:
        form=Input_Kegiatan()
    
    response = {
        'form' : form
    }

    return render(request, 'tambah_kegiatan.html', response)

def add_member(request, id):
    form = Input_Peserta(request.POST or None) 
    if form.is_valid():
        form.save()
        saved_peserta = Peserta.objects.all().last()
        saved_peserta.kegiatan = JadwalKegiatan.objects.get(pk=id)
        saved_peserta.save()
        form = Input_Peserta()
    else:
        form = Input_Peserta()
    
    response = {
        'form' : form,
        'id' : id,
    }

    return render(request, 'tambah_peserta.html', response)
